<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use App\Models\Employees;
use App\Models\ExportLogs;
use Session;
use Response;
use Request;

class DownloadController extends Controller
{
    public function exportCompanies()
    {
        $companies = Companies::withCount('employees')->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($company) {
            $company->created = date("F j, Y, g:i A", $company->created_at->timestamp); 
        });
        $csvExporter->build($companies, [
            'company_id' => 'Company ID',
            'company_name' => 'Company Name',
            'website' => 'Website',
            'employees_count' => 'Total Employees',
            'created' => 'Created'
        ]);

        $data = (string) $csvExporter->getCsv();
        
        if ($fileName = tempnam("/tmp", "company-dev")) {
            file_put_contents($fileName, $data);
            Session::put('download.companies-file', $fileName);
        }
        sleep(1); //emphasize

        return Response::json([true]);
    }
    public function exportEmployees()
    {
        $employees = Employees::with(array('company'=>function($query){
                                                          $query->select('company_id','company_name');
                                                      }))->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($company) {
            $company->created = date("F j, Y, g:i A", $company->created_at->timestamp); 
            $company->company_name = $company->company->company_name; 
        });
        $csvExporter->build($employees, [
            'full_name' => 'Employee Name',
            'employee_id' => 'Employee ID',
            'company_name' => 'Company Name',
            'company_id' => 'Company ID',
            'created' => 'Created'
        ]);

        $data = (string) $csvExporter->getCsv();
        if ($fileName = tempnam("/tmp", "company-dev")) {
            file_put_contents($fileName, $data);
            Session::put('download.employees-file', $fileName);
        }
        sleep(1); //emphasize

        return Response::json([true]);
    }
    public function downloadFile($downloadType)
    {
        if (!empty($fileName = Session::get('download.'.$downloadType.'-file'))) {
            $csvExporter = new \Laracsv\Export();
            if (file_exists($fileName)) {
                $downloadFilename = $downloadType . '-' . date('Y-m-d') . '.csv';
                Session::forget('download.'.$downloadType.'-file');

                $log                    = new ExportLogs();
                $log['export_type']     = $downloadType;
                $log['ip_address']      = Request::ip();;
                $log['user_agent']      = Request::server('HTTP_USER_AGENT');
                $log['file_name']       = $downloadFilename;
                $log->save();

                return Response::download($fileName, $downloadFilename)->deleteFileAfterSend(true);
            }            
        }

        return redirect('/')->withErrors(['message'=>'File not found']);
    }
}
