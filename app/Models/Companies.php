<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    public function employees()
    {
        return $this->hasMany('App\Models\Employees','company_id','company_id');
    }}
