<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    public function company()
    {
        return $this->belongsTo('App\Models\Companies','company_id','company_id');
    }
}
