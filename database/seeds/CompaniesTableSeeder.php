<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompaniesTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $faker = Faker::create();
        DB::table('companies')->delete();
        foreach (range(1,40) as $index) {
            DB::table('companies')->insert([
                'company_name' => $faker->company,
                'email' => $faker->email,
                'website' => $faker->domainName,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
