<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EmployeesTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $faker = Faker::create();
        DB::table('employees')->delete();
        foreach (range(1,140) as $index) {
            DB::table('employees')->insert([
                'company_id' => rand(1,40),
                'full_name' => $faker->name,
                'email' => $faker->email,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
