<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Index Page</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <style>
            .btn:focus,.btn:active {
                outline: none !important;
                box-shadow: none;
            }
        </style>
    </head>
    <body>
        <br><br>
        <div class="container">
            <div class="row col-sm-12">
                <div class="col-sm-5">
                    <h3><i class="fa fa-building"></i> Companies</h3>
                </div>
                <div class="col-sm-7 exports text-right">
                    <div class="float-lg-right">
                        <button data-url="/download/companies" data-type="companies" data-download="0" class="export-companies btn btn-primary text-white">Export Companies</button>
                        <button data-url="/download/employees" data-type="employees" data-download="0" class="export-employees btn btn-primary text-white">Export Employees</button>
                    </div>
                </div>
                @if (!empty(session('errors')))
                <div class="alert alert-danger">
                    {{session('errors')->first('message')}}
                </div>
                @endif
            </div>
        </div>
        <div class="container">
            <br />
            <table class="table table-striped">
                <thead class="text-center">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Website</th>
                        <th># of Employees</th>
                        <th>Created</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($companies as $company)
                    <tr>
                        <td>{{$company->company_id}}</td>
                        <td>{{$company->company_name}}</td>
                        <td>{{$company->website}}</td>
                        <td class="text-center">{{$company->employees()->count()}}</td>
                        <td class="text-center">{{$company->created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var exportsContainer = $('.exports');
            exportsContainer.on('click', '.export-companies, .export-employees', function(){
                var el = $(this);
                if(el.data('download') == '1') {
                    el.attr('disabled', true);
                    document.location.href = el.data('url');
                } else {
                    $.ajax({
                        method: "POST",
                        url:'/download/export-'+el.data('type'),
                        data: {limit: 0},
                        beforeSend: function( xhr ) {
                            el.html('Exporting...</i>').attr('disabled',true);
                        }
                    })
                    .done(function( r ) {
                        if (r[0]) {
                            el.html('Download ' + el.data('type') + ' (CSV) <i class="fa fa-arrow-alt-circle-down"></i>')
                            el.data('download', 1);
                            el.data('url', '/download/download-file-' + el.data('type')).attr('disabled',false);
                        } else {
                            document.location.reload()
                        }
                    });
                }
            })
        })
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
</html>