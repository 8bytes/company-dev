<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'HomeController');

Route::post('/download/export-companies', 'DownloadController@exportCompanies');
Route::post('/download/export-employees', 'DownloadController@exportEmployees');

Route::get('/download/download-file-{type}', 'DownloadController@downloadFile');
